package com.alsnightsoft.vaadin.containers.demo;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.alsnightsoft.vaadin.containers.LazyQuery;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("lazypagedcontainer")
public class LazypagedcontainerUI extends UI {

	private VerticalLayout mainLayout;

	private DemoDomainContainer container;
	private ServiceDemo service = new ServiceDemo();

	private TextField tfFilterName;
	private CheckBox chkEnable;
	private Grid tableDemo;

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = LazypagedcontainerUI.class, widgetset = "com.alsnightsoft.vaadin.containers.demo.widgetset.LazypagedcontainerWidgetset")
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		mainLayout = new VerticalLayout();

		container = new DemoDomainContainer();

		tfFilterName = new TextField("Filter");
		tfFilterName.setImmediate(true);
		tfFilterName.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
				if (tfFilterName.getValue().equals("")) {
					container.setApplyLazyFilter(false);
				} else {
					container.setApplyLazyFilter(true);
				}
				tableDemo.clearSortOrder();
			}
		});

		chkEnable = new CheckBox("Show Disable");
		chkEnable.setImmediate(true);
		chkEnable.setValue(false);
		chkEnable.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
				tableDemo.clearSortOrder();
			}
		});

		container.setLazyQuery(new LazyQuery<DemoDomain>() {
			@Override
			public int getLazySize() {
				return service.count(chkEnable.getValue());
			}

			@Override
			public List<DemoDomain> getLazyItemsIds(int startIndex,
					int numberOfIds) {
				return service.getItemsFromTo(chkEnable.getValue(),
						startIndex, numberOfIds);
			}

			@Override
			public int getLazyFilteredSize() {
				return service.countFilter(chkEnable.getValue(),
						tfFilterName.getValue());
			}

			@Override
			public List<DemoDomain> getLazyFilteredItemsIds(int startIndex,
					int numberOfIds) {
				return service.getFilterItemsFromTo(chkEnable.getValue(),
						tfFilterName.getValue(), startIndex, numberOfIds);
			}
		});
		// disable use filter query (default false)
		container.setApplyLazyFilter(false);

		tableDemo = new Grid();
		tableDemo.setImmediate(true);
		tableDemo.setSizeFull();
		tableDemo.setFooterVisible(true);
		tableDemo.setContainerDataSource(container);
		tableDemo.setColumnOrder("id", "name");
		tableDemo.getColumn("id").setHeaderCaption("ID");
		tableDemo.getColumn("name").setHeaderCaption("Name");
		tableDemo.removeColumn("enabled");
		tableDemo.addSelectionListener(new SelectionListener() {
			@Override
			public void select(SelectionEvent event) {
				System.out.println("Change Value");
				System.out.println(((DemoDomain) tableDemo.getSelectedRow()).getName());
			}
		});
	
		mainLayout.addComponent(tfFilterName);
		mainLayout.addComponent(chkEnable);
		mainLayout.addComponent(tableDemo);

		// When add or update data on DB use that to refresh Table
		// Can use filter on the table if do it when add news values to the db.
		// Only filter items on the view, other items need a filter query on the
		// LazyQuery
		tableDemo.clearSortOrder();

		setContent(mainLayout);
	}

}