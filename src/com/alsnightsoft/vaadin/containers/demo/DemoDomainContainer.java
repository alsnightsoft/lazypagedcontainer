package com.alsnightsoft.vaadin.containers.demo;

import com.alsnightsoft.vaadin.containers.LazyPagedContainer;

public class DemoDomainContainer extends LazyPagedContainer<DemoDomain> {

	private static final long serialVersionUID = 3018567394091518443L;

	public DemoDomainContainer() {
		super(DemoDomain.class);
	}

}
