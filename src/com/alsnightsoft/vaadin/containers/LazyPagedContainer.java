package com.alsnightsoft.vaadin.containers;

import java.util.List;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;

public class LazyPagedContainer<BEANTYPE> extends BeanItemContainer<BEANTYPE> {

	private static final long serialVersionUID = 6183223215084402510L;
	private LazyQuery<BEANTYPE> lazyQuery;
	private Boolean applyLazyFilter = false;

	public LazyPagedContainer(Class<? super BEANTYPE> type)
			throws IllegalArgumentException {
		super(type);
	}
	
	@Override
	public int size() {
		if (applyLazyFilter) {
			return lazyQuery.getLazyFilteredSize();
		}
		return lazyQuery.getLazySize();
	}

	@SuppressWarnings("unchecked")
	@Override
	public BeanItem<BEANTYPE> getItem(Object itemId) {
		return new BeanItem<BEANTYPE>(((BEANTYPE) itemId));
	}
	
	@Override
	public List<BEANTYPE> getItemIds(int startIndex, int numberOfIds) {
		if (applyLazyFilter) {
			return lazyQuery.getLazyFilteredItemsIds(startIndex, numberOfIds);
		}
		return lazyQuery.getLazyItemsIds(startIndex, numberOfIds);
	}
	
	@Override
	public BEANTYPE getIdByIndex(int index) {
		if (applyLazyFilter) {
			return lazyQuery.getLazyFilteredItemsIds(index, 1).get(0);
		}
		return lazyQuery.getLazyItemsIds(index, 1).get(0);
	}

	@Override
	public boolean containsId(Object itemId) {
		return true;
	}
	
	public LazyQuery<BEANTYPE> getLazyQuery() {
		return lazyQuery;
	}

	public void setLazyQuery(LazyQuery<BEANTYPE> lazyQuery) {
		this.lazyQuery = lazyQuery;
	}

	public Boolean getApplyLazyFilter() {
		return applyLazyFilter;
	}

	public void setApplyLazyFilter(Boolean applyLazyFilter) {
		this.applyLazyFilter = applyLazyFilter;
	}

}
