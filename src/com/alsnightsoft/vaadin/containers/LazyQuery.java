package com.alsnightsoft.vaadin.containers;

import java.util.List;

public interface LazyQuery<BEANTYPE> {

	/**
     * Get Size by a Query
     */
    public int getLazySize();

    /**
     * Get items to view on the page using the same model of the container.
     */
    public List<BEANTYPE> getLazyItemsIds(int startIndex, int numberOfIds);

    /**
     * Get Size for manual filter.
     */
    public int getLazyFilteredSize();

    /**
     * Get items to view on the page for manual filter using the same model of the container.
     */
    public List<BEANTYPE> getLazyFilteredItemsIds(int startIndex, int numberOfIds);
    
}
